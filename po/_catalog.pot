# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-12 14:13+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#. Generate the string for the browser's windows in the art slideshow viewer
#. %1$s = artwork title, %2$s = author
#. Image (big)
#. -----------
#: index.php:283 core/viewer.php:64 core/viewer.php:84
#, php-format
msgid "%1$s by %2$s"
msgstr ""

#: index.php:283
msgid "Viewer"
msgstr ""

#. In other case, redirect mode to better Gettext translated labels
#: index.php:292
msgid "Philosophy"
msgstr ""

#: index.php:293 index.php:314 core/artworks.php:24 core/mod-footer.php:89
#: core/mod-header.php:110
msgid "Artworks"
msgstr ""

#: index.php:294 index.php:326 core/mod-footer.php:68
msgid "Wiki"
msgstr ""

#: index.php:295
msgid "Documentation"
msgstr ""

#: index.php:296 core/mod-footer.php:83
msgid "Homepage"
msgstr ""

#: index.php:297
msgid "Files"
msgstr ""

#: index.php:298 core/mod-footer.php:86 core/mod-header.php:105
msgid "Webcomics"
msgstr ""

#: index.php:299 core/mod-footer.php:92 core/mod-header.php:115
msgid "Goodies"
msgstr ""

#: index.php:300 core/mod-footer.php:95 core/mod-header.php:120
msgid "Contribute"
msgstr ""

#: index.php:301 index.php:318
msgid "Fan-art"
msgstr ""

#: index.php:302 index.php:325 core/mod-footer.php:77 core/wallpapers.php:10
msgid "Wallpapers"
msgstr ""

#: index.php:303
msgid "Website setup"
msgstr ""

#: index.php:304 core/mod-footer.php:104 core/mod-header.php:125
msgid "About"
msgstr ""

#: index.php:305
msgid "Support"
msgstr ""

#: index.php:306 core/mod-footer.php:107 core/mod-header.php:130
msgid "License"
msgstr ""

#: index.php:307
msgid "Chat rooms"
msgstr ""

#: index.php:308 core/mod-footer.php:110
msgid "Terms of Services and Privacy"
msgstr ""

#: index.php:315
msgid "Book-publishing"
msgstr ""

#: index.php:316
msgid "Commissions"
msgstr ""

#: index.php:317 core/mod-footer.php:98 core/mod-header.php:134
msgid "Shop"
msgstr ""

#: index.php:319
msgid "Framasoft"
msgstr ""

#: index.php:320 core/artworks.php:34
msgid "Misc"
msgstr ""

#: index.php:321
msgid "Press"
msgstr ""

#: index.php:322
msgid "References"
msgstr ""

#: index.php:323 core/artworks.php:29
msgid "Sketchbook"
msgstr ""

#: index.php:324
msgid "Vector"
msgstr ""

#: index.php:331
msgid "Not found:"
msgstr ""

#: core/404.php:7
msgid "Error 404: Page not found."
msgstr ""

#: core/about.php:15 core/tos.php:11
msgid ""
"Photo of the author, David Revoy with his cat named Noutti while drawing the "
"first episodes of Pepper&Carrot."
msgstr ""

#: core/about.php:17
msgid "The author"
msgstr ""

#: core/about.php:19
msgid ""
"Hi, my name is David Revoy and I'm a French artist born in 1981. I'm self-"
"taught and passionate about drawing, painting, cats, computers, Gnu/Linux "
"open-source culture, Internet, old school RPG video-games, old mangas and "
"anime, traditional art, Japanese culture, fantasy…"
msgstr ""

#: core/about.php:20
msgid ""
"After more than 10 years of freelance in digital painting, teaching, concept-"
"art, illustrating and art-direction, I decided to start my own project. I "
"finally found a way to mix all my passions together, the result is "
"Pepper&Carrot."
msgstr ""

#: core/about.php:21
msgid ""
"I'm working on this project since May 2014, and I received the help of many "
"contributors and supporters on the way. "
msgstr ""

#: core/about.php:22
#, php-format
msgid ""
"You'll find the full list of credits on the <a href=\"%s\">License menu</a>."
msgstr ""

#: core/about.php:24
msgid "My blog: "
msgstr ""

#: core/about.php:25
msgid "My email: "
msgstr ""

#: core/about.php:29
msgid "The philosophy of Pepper&Carrot"
msgstr ""

#: core/about.php:31
msgid "Supported by patrons"
msgstr ""

#: core/about.php:33
msgid ""
"Pepper&Carrot project is only funded by its patrons, from all around the "
"world. Each patron sends a little money for each new episode published and "
"gets a credit at the end of the new episode. Thanks to this system, "
"Pepper&Carrot can stay independent and never have to resort to advertising "
"or any marketing pollution."
msgstr ""

#: core/about.php:35
msgid "Pepper and Carrot receiving money from the audience."
msgstr ""

#: core/about.php:37
msgid "100&#37; free(libre), forever, no paywall"
msgstr ""

#: core/about.php:39
msgid ""
"All the content I produce about Pepper&Carrot is on this website or on my "
"blog, free(libre) and available to everyone. I respect all of you equally: "
"with or without money. All the goodies I make for my patrons are also posted "
"here. Pepper&Carrot will never ask you to pay anything or to get a "
"subscription to get access to new content."
msgstr ""

#: core/about.php:41
msgid "Carrot, locked behind a paywall."
msgstr ""

#: core/about.php:44
msgid "Open-source and permissive"
msgstr ""

#: core/about.php:46
msgid ""
"I want to give people the right to share, use, build and even make money "
"upon the work I've created. All pages, artworks and content were made with "
"Free(Libre) Open-Sources Software on GNU/Linux, and all sources are on this "
"website (Sources and License buttons). Commercial usage, translations, fan-"
"art, prints, movies, video-games, sharing, and reposts are encouraged. You "
"just need to give appropriate credit to the authors (artists, correctors, "
"translators involved in the artwork you want to use), provide a link to the "
"license, and indicate if changes were made. You may do so in any reasonable "
"manner, but not in any way that suggests the authors endorse you or your "
"use. More information can be read about it here: "
msgstr ""

#: core/about.php:46
msgid "Creative Commons Attribution 4.0 International license."
msgstr ""

#: core/about.php:48
msgid "Example of derivatives possible."
msgstr ""

#: core/about.php:50
msgid "Quality entertainment for everyone, everywhere"
msgstr ""

#: core/about.php:52
msgid ""
"Pepper&Carrot is a comedy/humor webcomic suited for everyone, every age. No "
"mature content, no violence. Free(libre) and open-source, Pepper&Carrot is a "
"proud example of how cool free-culture can be. I focus a lot on quality, "
"because free(libre) and open-source doesn't mean bad or amateur. Quite the "
"contrary."
msgstr ""

#: core/about.php:54
msgid "Comic pages around the world."
msgstr ""

#: core/about.php:56
msgid "Let's change comic industry!"
msgstr ""

#: core/about.php:58
msgid ""
"Without intermediary between artist and audience you pay less and I benefit "
"more. You support me directly. No publisher, distributor, marketing team or "
"fashion police can force me to change Pepper&Carrot to fit their vision of "
"'the market'. Why couldn't a single success 'snowball' to a whole industry "
"in crisis? We'll see…"
msgstr ""

#: core/about.php:60
msgid ""
"Diagram: on the left-hand side, Carrot is losing money with many middle-men. "
"On the right-hand side, the result is more balanced."
msgstr ""

#: core/about.php:63 core/homepage.php:27 core/homepage.php:28
#: core/mod-header.php:148
msgid "Become a patron"
msgstr ""

#: core/artworks.php:38 core/contribute.php:34 core/files.php:168
msgid "Sources explorer"
msgstr ""

#: core/contribute.php:13
#, php-format
msgid ""
"Thanks to the <a href=\"%s\">Creative Commons license</a>, you can "
"contribute Pepper&Carrot in many ways."
msgstr ""

#: core/contribute.php:14
#, php-format
msgid ""
"Join the <a href=\"%s\">Pepper&Carrot chat room</a> to share your projects!"
msgstr ""

#: core/contribute.php:15
msgid ""
"(Note: English language is used accross our documentations, wiki and "
"channels.)"
msgstr ""

#: core/contribute.php:28
msgid "Your Derivations"
msgstr ""

#: core/contribute.php:29
msgid "Fan-art Gallery"
msgstr ""

#: core/contribute.php:30
msgid "Translation Documentation"
msgstr ""

#: core/contribute.php:31
msgid "Beta-reading Forum"
msgstr ""

#: core/contribute.php:32
msgid "Wiki: Document the Universe"
msgstr ""

#: core/contribute.php:33
msgid "Cosplay"
msgstr ""

#: core/contribute.php:35
msgid "Git repositories"
msgstr ""

#: core/fan-art.php:32
#, php-format
msgid "%d Fan-art"
msgid_plural "%d Fan-art"
msgstr[0] ""
msgstr[1] ""

#: core/fan-art.php:37 core/files.php:58
msgid "Fan comics"
msgstr ""

#. Fan Fiction main menu
#: core/fan-art.php:42 core/fan-art.php:101 core/files.php:59
msgid "Fan Fiction"
msgstr ""

#: core/fan-art.php:47
msgid "How to:"
msgstr ""

#: core/fan-art.php:48
#, php-format
msgid ""
"Send me your artworks, comics and fiction using social-medias (tag me, my "
"social media links are written in the footer of this website) or send them "
"to me on <a href=\"%s\">the chat room</a> and I'll repost them here. "
msgstr ""

#: core/fan-art.php:76
msgid "External history link to see all changes made to this page"
msgstr ""

#: core/fan-art.php:78
msgid "View history"
msgstr ""

#: core/fan-art.php:83
msgid "Edit this page with an external editor"
msgstr ""

#: core/fan-art.php:85
msgid "Edit"
msgstr ""

#. Sidebar menu
#. ============
#. Dynamic services:
#: core/files.php:38
msgid "Episodes"
msgstr ""

#: core/files.php:39
msgid "All Comic Panels"
msgstr ""

#: core/goodies.php:22
msgid "Free Wallpapers"
msgstr ""

#: core/goodies.php:23
msgid "Free Brushes and Resources"
msgstr ""

#: core/goodies.php:24
msgid "Free Tutorials and Making-of"
msgstr ""

#: core/goodies.php:25
msgid "Free Avatar Generators"
msgstr ""

#: core/goodies.php:26
msgid "Free Install Guides for Linux and Tablets"
msgstr ""

#: core/goodies.php:27
msgid "Free Pepper&Carrot Fonts"
msgstr ""

#: core/homepage.php:22
msgid ""
"A free(libre) and open-source webcomic supported directly by its patrons to "
"change the comic book industry!"
msgstr ""

#: core/homepage.php:24 core/homepage.php:25
msgid "Read the webcomic for free"
msgstr ""

#: core/homepage.php:36
msgid "Latest episode"
msgstr ""

#. In case the cover is not available in the current language, fallback to English.
#: core/homepage.php:55
#, php-format
msgid "Click to read episode %d."
msgstr ""

#: core/homepage.php:69
#, php-format
msgid "Show %d episode"
msgid_plural "Show all %d episodes"
msgstr[0] ""
msgstr[1] ""

#: core/homepage.php:79
msgid "Recent blog-posts"
msgstr ""

#: core/homepage.php:137
msgid "(Note: in English only)"
msgstr ""

#: core/homepage.php:138
msgid "Go to the blog"
msgstr ""

#: core/homepage.php:145
msgid "Support Pepper&Carrot on"
msgstr ""

#: core/homepage.php:151
msgid "More options"
msgstr ""

#: core/lib-credits.php:36
msgid "Art:"
msgstr ""

#: core/lib-credits.php:41
msgid "Scenario:"
msgstr ""

#: core/lib-credits.php:47
msgid "Script-doctor:"
msgstr ""

#: core/lib-credits.php:53
msgid "Inspiration:"
msgstr ""

#: core/lib-credits.php:59
msgid "Beta-readers:"
msgstr ""

#: core/lib-credits.php:66
msgid "(original version)"
msgstr ""

#: core/lib-credits.php:69 core/lib-credits.php:168
msgid "Translation:"
msgstr ""

#: core/lib-credits.php:75 core/lib-credits.php:174
msgid "Proofreading:"
msgstr ""

#: core/lib-credits.php:81 core/lib-credits.php:180
msgid "Contribution:"
msgstr ""

#: core/lib-credits.php:87
msgid "Notes:"
msgstr ""

#: core/lib-credits.php:123
msgid "published on"
msgstr ""

#: core/lib-credits.php:139
msgid "Creation:"
msgstr ""

#: core/lib-credits.php:142
msgid "Lead Maintainer:"
msgstr ""

#: core/lib-credits.php:145
msgid "Writers:"
msgstr ""

#: core/lib-credits.php:148
msgid "Correctors:"
msgstr ""

#: core/lib-credits.php:161
msgid "Website translation:"
msgstr ""

#: core/lib-credits.php:195
msgid "Technical maintenance and scripting:"
msgstr ""

#: core/lib-credits.php:198
msgid "General maintenance of the database of SVGs:"
msgstr ""

#: core/lib-credits.php:201
msgid "Website maintenance and new features:"
msgstr ""

#: core/lib-navigation.php:93
msgid "First"
msgstr ""

#: core/lib-navigation.php:94
msgid "Previous"
msgstr ""

#: core/lib-navigation.php:95
msgid "Next"
msgstr ""

#: core/lib-navigation.php:96
msgid "Last"
msgstr ""

#: core/license.php:13 core/mod-webcomic-sources.php:62 core/viewer.php:142
msgid "License:"
msgstr ""

#. A note here, because the Copyright © will probably rise eyebrows on a Free/Libre and Open-source Project.
#. It is advised to declare a 'copyright holder' before also declaring releasing as Creative Commons for some copyright code of some countries.
#. Note to translators: The %s placeholders are an opening and a closing link tag
#: core/license.php:18
#, php-format
msgid ""
"This work is licensed under a %sCreative Commons Attribution 4.0 "
"International license%s"
msgstr ""

#: core/license.php:19
msgid "Copyright © David Revoy"
msgstr ""

#: core/license.php:21
msgid "Here is the total list of attributions for the selected language:"
msgstr ""

#: core/license.php:23
msgid "Full attribution:"
msgstr ""

#: core/license.php:26
msgid "Project management:"
msgstr ""

#: core/license.php:32
msgid "Hereva worldbuilding:"
msgstr ""

#: core/license.php:36
msgid "Episodes:"
msgstr ""

#. Special thanks:
#. ------------------
#: core/license.php:49
msgid "Special thanks to:"
msgstr ""

#. to all translators
#. ------------------
#: core/license.php:52
msgid ""
"All patrons of Pepper&amp;Carrot! Your support made all of this possible."
msgstr ""

#. to all translators
#. ------------------
#: core/license.php:56
msgid "All translators on Pepper&amp;Carrot for their contributions:"
msgstr ""

#. to the projects
#. ----------------
#: core/license.php:86
msgid ""
"<strong>The Framasoft team</strong> for hosting our oversized repositories "
"via their Gitlab instance Framagit."
msgstr ""

#: core/license.php:90
msgid ""
"<strong>The Libera.Chat staff and Matrix.org staff</strong> for our "
"#pepper&carrot community channel."
msgstr ""

#: core/license.php:94
msgid ""
"<strong>All Free/Libre and open-source software</strong> because "
"Pepper&Carrot episodes are created using 100&#37; Free/Libre software on a "
"GNU/Linux operating system. The main ones used in production being:"
msgstr ""

#: core/license.php:95
msgid "- <strong>Krita</strong> for artworks (krita.org)."
msgstr ""

#: core/license.php:96
msgid ""
"- <strong>Inkscape</strong> for vector and speechbubbles (inkscape.org)."
msgstr ""

#: core/license.php:97
msgid ""
"- <strong>Blender</strong> for artworks and video editing (blender.org)."
msgstr ""

#: core/license.php:98
msgid "- <strong>Kdenlive</strong> for video editing (kdenlive.org)."
msgstr ""

#: core/license.php:99
msgid "- <strong>Scribus</strong> for the book project (scribus.net)."
msgstr ""

#: core/license.php:100
msgid "- <strong>Gmic</strong> for filters and effects (gmic.eu)."
msgstr ""

#: core/license.php:101
msgid ""
"- <strong>ImageMagick</strong> & <strong>Bash</strong> for 90&#37; of "
"automation on the project."
msgstr ""

#: core/license.php:107
msgid ""
"And finally to all developers who interacted on fixing Pepper&Carrot "
"specific bug-reports:"
msgstr ""

#: core/license.php:109
msgid "... and anyone I've missed."
msgstr ""

#: core/mod-footer.php:6
msgid "Follow Pepper&Carrot on:"
msgstr ""

#: core/mod-footer.php:25
msgid "Join community chat rooms:"
msgstr ""

#: core/mod-footer.php:27
msgid "IRC: #pepper&carrot on libera.chat"
msgstr ""

#: core/mod-footer.php:71
msgid "Making-of"
msgstr ""

#: core/mod-footer.php:74
msgid "Brushes"
msgstr ""

#: core/mod-footer.php:101 core/mod-header.php:138
msgid "Blog"
msgstr ""

#: core/mod-footer.php:113
msgid "Code of Conduct"
msgstr ""

#. Prepare strings that will get reused in header:
#: core/mod-header.php:4
msgid "Pepper&amp;Carrot"
msgstr ""

#: core/mod-header.php:7
msgid ""
"Official homepage of Pepper&amp;Carrot, a free(libre) and open-source "
"webcomic about Pepper, a young witch and her cat, Carrot. They live in a "
"fantasy universe of potions, magic, and creatures."
msgstr ""

#. %1$s is the locale code, %2$s is the language name in English
#: core/mod-menu-lang.php:126
#, php-format
msgid "%1$s/%2$s translation (bookmarked)"
msgstr ""

#. %1$s is the locale code, %2$s is the language name in English
#: core/mod-menu-lang.php:129
#, php-format
msgid "%1$s/%2$s translation"
msgstr ""

#: core/mod-menu-lang.php:145
#, php-format
msgid "%d language"
msgid_plural "all %d languages"
msgstr[0] ""
msgstr[1] ""

#. Menu 2: full list with percents:
#. --------------------------------
#. Define some translatable strings that will be reused.
#. This will save execution time on fetching the translations with gettext.
#. I am splitting off the website info in case we want to get a completion percentage
#. there in the future - this will save retranslation effort.
#. Placeholders: %1$s = language name, %2$s = locale code
#: core/mod-menu-lang.php:161
#, php-format
msgid "%1$s (%2$s): The translation is complete."
msgstr ""

#. Website translation is at 100%
#: core/mod-menu-lang.php:163
msgid "The website has been translated."
msgstr ""

#. Website translation needs work
#: core/mod-menu-lang.php:165
msgid "The website is being translated."
msgstr ""

#. Website translation is below minimum completion
#: core/mod-menu-lang.php:167
msgid "The website has not been translated yet."
msgstr ""

#: core/mod-menu-lang.php:229
msgid "The star congratulates a 100&#37; complete translation."
msgstr ""

#. Combine website completion statement with comic percentage string to assemble tooltip
#. Placeholders: %1$s = language name, %2$s = locale code, %3$d = percentage (plural controller), %4$s = Sentence explaining website completion status. &#37; is the % symbol
#: core/mod-menu-lang.php:244
#, php-format
msgid "%1$s (%2$s): Comics %3$d&#37; translated. %4$s"
msgid_plural "%1$s (%2$s): Comics %3$d&#37; translated. %4$s"
msgstr[0] ""
msgstr[1] ""

#: core/mod-menu-lang.php:265
#, php-format
msgid "Save %s as favorite language"
msgstr ""

#: core/mod-menu-lang.php:270
msgid "Add a translation"
msgstr ""

#: core/mod-webcomic-sources.php:30
msgid ""
"Comic pages of Pepper&Carrot comes from two sources: the illustration and "
"the text."
msgstr ""

#: core/mod-webcomic-sources.php:31
msgid ""
"This page offers links to download them, but also to download ready to use "
"compiled rendering."
msgstr ""

#: core/mod-webcomic-sources.php:43
msgid "Cover of the episode"
msgstr ""

#: core/mod-webcomic-sources.php:54
msgid "Git directory"
msgstr ""

#: core/mod-webcomic-sources.php:55
msgid "Git history"
msgstr ""

#: core/mod-webcomic-sources.php:64
msgid ""
"If you republish this episode, you need to provide the following information:"
msgstr ""

#: core/mod-webcomic-sources.php:65 core/viewer.php:145
msgid "Creative Commons Attribution 4.0 International license"
msgstr ""

#: core/mod-webcomic-sources.php:66
msgid "Attribution to:"
msgstr ""

#: core/mod-webcomic-sources.php:68
msgid "Credit for the universe of Pepper&Carrot, Hereva:"
msgstr ""

#: core/mod-webcomic-sources.php:70
msgid ""
"Note: these credits are different depending the episode selected and the "
"language."
msgstr ""

#: core/mod-webcomic-sources.php:71 core/viewer.php:147
#, php-format
msgid ""
"More information and good practice for attribution can be found <a href=\"%s"
"\">on the documentation</a>."
msgstr ""

#. Single-page
#: core/mod-webcomic-sources.php:76
msgid "Collage of all pages into a single image file"
msgstr ""

#: core/mod-webcomic-sources.php:92
msgid "<strong>Illustration sources</strong>: Krita (kra) pages."
msgstr ""

#: core/mod-webcomic-sources.php:105
msgid "<strong>Text sources:</strong> Inkscape (svg) pages."
msgstr ""

#: core/mod-webcomic-sources.php:122
msgid "Ready to use compiled renderings:"
msgstr ""

#: core/mod-webcomic-sources.php:123
msgid "High quality and recommended for printing.<br/><br/>"
msgstr ""

#. Adapt captions to the type of page
#: core/mod-webcomic-sources.php:151 core/webcomic.php:177
#: core/webcomic.php:178 core/xyz.php:51 core/xyz.php:52
msgid "Page"
msgstr ""

#: core/mod-webcomic-sources.php:153
msgid "Credits"
msgstr ""

#: core/mod-webcomic-sources.php:156 core/webcomic.php:159 core/xyz.php:36
msgid "Header"
msgstr ""

#: core/mod-webcomic-sources.php:159
msgid "Cover"
msgstr ""

#: core/mod-webcomic-sources.php:161 core/mod-webcomic-sources.php:186
msgid "click to enlarge."
msgstr ""

#: core/mod-webcomic-sources.php:170
msgid "Illustration and text"
msgstr ""

#: core/mod-webcomic-sources.php:171
msgid "Only illustration"
msgstr ""

#: core/mod-webcomic-sources.php:172
msgid "Only text"
msgstr ""

#: core/mod-webcomic-sources.php:185
msgid "Gif"
msgstr ""

#: core/mod-webcomic-sources.php:195
msgid "Animation"
msgstr ""

#: core/mod-webcomic-sources.php:196
msgid "Static version for print"
msgstr ""

#: core/setup.php:14
msgid "Return to the previous page"
msgstr ""

#: core/setup.php:16
msgid "Language saved as your favorite!"
msgstr ""

#: core/support.php:16
msgid "Support Pepper&Carrot on:"
msgstr ""

#: core/support.php:19
msgid "Patreon"
msgstr ""

#: core/support.php:22
msgid "Other possibilities:"
msgstr ""

#: core/support.php:29
msgid "Wire transfer:"
msgstr ""

#: core/support.php:35
msgid "Address for gifts:"
msgstr ""

#: core/viewer.php:58
msgid "source and license"
msgstr ""

#. Close button
#: core/viewer.php:69 core/viewer.php:96
msgid "Return to the gallery of thumbnails."
msgstr ""

#: core/viewer.php:115
msgid "Artist:"
msgstr ""

#: core/viewer.php:116
msgid "Original size:"
msgstr ""

#: core/viewer.php:117
msgid "Created on:"
msgstr ""

#: core/viewer.php:118
msgid "Category:"
msgstr ""

#: core/viewer.php:126
msgid "Download the high resolution picture"
msgstr ""

#: core/viewer.php:133
msgid "Download the source file (zip)"
msgstr ""

#: core/viewer.php:146
msgid "Attribution to"
msgstr ""

#. fan-art license exception
#: core/viewer.php:150
#, php-format
msgid ""
"This picture is fan-art made by %s. It is reposted on the fan-art gallery of "
"Pepper&Carrot with permission."
msgstr ""

#: core/viewer.php:151
msgid ""
"Do not reuse this picture for your project unless you obtain author's "
"permissions."
msgstr ""

#: core/webcomic.php:83
msgid "Display the transcript for this episode."
msgstr ""

#: core/webcomic.php:109
msgid "No transcript available for this episode."
msgstr ""

#: core/webcomic.php:129
msgid "Display the pages of this episode in high resolution."
msgstr ""

#: core/webcomic.php:129
msgid "High resolution"
msgstr ""

#: core/webcomic.php:130
msgid "Transcript"
msgstr ""

#: core/webcomic.php:131
msgid ""
"Display the full set of sources files for this episode in this language."
msgstr ""

#: core/webcomic.php:131
msgid "Sources and license"
msgstr ""

#: core/webcomic.php:135
msgid ""
"Oops! There is no translation available yet for this episode with the "
"language you selected. The page will continue in English."
msgstr ""

#: core/webcomic.php:205 core/webcomics.php:62
#, php-format
msgid "Read the %d comment on the blog."
msgid_plural "Read the %d comments on the blog."
msgstr[0] ""
msgstr[1] ""

#: core/webcomic.php:206
#, php-format
msgid "Read %d comment."
msgid_plural "Read %d comments."
msgstr[0] ""
msgstr[1] ""

#: core/webcomics.php:9
msgid "All episodes"
msgstr ""

#: core/webcomics.php:57
msgid "(click to open the episode)"
msgstr ""

#: core/webcomics.php:62
#, php-format
msgid "Published on %1$s, <a %2$s>%3$d comment</a>."
msgid_plural "Published on %1$s, <a %2$s>%3$d comments</a>."
msgstr[0] ""
msgstr[1] ""
