<?php if ($root=="") exit;

echo '<div class="container">'."\n";
echo '  <main class="main grid" role="main">'."\n";
echo '    <section class="col sml-12 med-12 lrg-12 sml-text-center">'."\n";

echo '      <h1>'._("Error 404: Page not found.").'</h1><br/>'."\n";
echo '      <img src="'.$root.'/'.$sources.'/0ther/artworks/hi-res/2019-10-17_chicory_by-David-Revoy.jpg" /><br/>'."\n";

echo '    </section>'."\n";
echo '  </main>'."\n";
echo '</div>'."\n";
?>
