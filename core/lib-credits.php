<?php

# lib-credits.php
# -------------
# Lib-credits contains function to ease printing the full credits for an episode
# Require also functions of lib-database.
# Usage: just include the file on the header of the code.
#
# @author: David Revoy, GunChleoc
#
# @license: http://www.gnu.org/licenses/gpl.html GPL version 3 or higher

# Print a tiny paragraph that include all the credits
# for a $lang and a $episode.

function _print_credits($lang, $epdirectory) {

  global $sources;
  global $languages_info;
  $projectpath = $sources.'/'.$epdirectory;

  if (file_exists($projectpath.'/info.json')) {
    if (file_exists($projectpath.'/lang/'.$lang.'/info.json')) {
      if (file_exists($projectpath.'/hi-res/titles.json')) {

        $foldername = basename($projectpath);
        # Read all infos from json on 0_sources:
        $episodeinfos = json_decode(file_get_contents($projectpath.'/info.json'), true);
        $episodetitle = json_decode(file_get_contents($projectpath.'/hi-res/titles.json'), true);
        $translatorinfos = json_decode(file_get_contents($projectpath.'/lang/'.$lang.'/info.json'), true);
        $localname = $languages_info[$lang]['local_name'];

        # Art
        if (isset($episodeinfos['credits']['art'])) {
          echo '<br/>';
          echo ''._("Art:").' ';
          _print_translatorinfos($episodeinfos['credits']['scenario'], ", ", ".").'';
        }
        # Scenario
        if (isset($episodeinfos['credits']['scenario'])) {
          echo '&nbsp;&nbsp;&nbsp;'._("Scenario:").' ';
          _print_translatorinfos($episodeinfos['credits']['scenario'], ", ", ".").'';
        }
        # script-doctor
        if (isset($episodeinfos['credits']['script-doctor'])) {
          echo '<br/>';
          echo ''._("Script-doctor:").' ';
          _print_translatorinfos($episodeinfos['credits']['script-doctor'], ", ", ".").'';
        }
        # inspiration
        if (isset($episodeinfos['credits']['inspiration'])) {
          echo '<br/>';
          echo ''._("Inspiration:").' ';
          _print_translatorinfos($episodeinfos['credits']['inspiration'], ", ", ".").'';
        }
        # Beta-readers
        if (isset($episodeinfos['credits']['beta-readers'])) {
          echo '<br/>';
          echo ''._("Beta-readers:").' ';
          _print_translatorinfos($episodeinfos['credits']['beta-readers'], ", ", ".").'';
        }
        if (isset($translatorinfos['credits'])) {
          if (isset($translatorinfos['credits']['translation'])) {
            if (in_array('original version', $translatorinfos['credits']['translation'])) {
              echo '<br/>';
              echo ''.$localname.' '._("(original version)").'';
            } else {
              echo '<br/>';
              echo ''._("Translation:").'&nbsp;['.$localname.']'.' ';
              _print_translatorinfos($translatorinfos['credits']['translation'], ", ", ".").'';
            }
          }
          if (isset($translatorinfos['credits']['proofreading'])) {
            echo '<br/>';
            echo ''._("Proofreading:").' ';
            _print_translatorinfos($translatorinfos['credits']['proofreading'], ", ", ".").'';
          }

          if (isset($translatorinfos['credits']['contribution'])) {
            echo '<br/>';
            echo ''._("Contribution:").' ';
            _print_translatorinfos($translatorinfos['credits']['contribution'], ", ", ".").'';
          }

          if (isset($translatorinfos['credits']['notes'])) {
            echo '<br/>';
            echo ''._("Notes:").' ';
            _print_translatorinfos($translatorinfos['credits']['notes'], " ", " ").'';
          }
        }
      echo '<br/>'."\n";
      echo '<br/>'."\n";
      }
    }
  }

}

# Print a tiny paragraph that include all the credits
# for a $lang and a $episode.
function _print_title($lang, $epdirectory) {

  global $sources;
  $projectpath = $sources.'/'.$epdirectory;

  if (file_exists($projectpath.'/info.json')) {
    if (file_exists($projectpath.'/lang/'.$lang.'/info.json')) {
      if (file_exists($projectpath.'/hi-res/titles.json')) {

        $foldername = basename($projectpath);
        # Read all infos from json on 0_sources:
        $episodeinfos = json_decode(file_get_contents($projectpath.'/info.json'), true);
        $episodetitle = json_decode(file_get_contents($projectpath.'/hi-res/titles.json'), true);
        $translatorinfos = json_decode(file_get_contents($projectpath.'/lang/'.$lang.'/info.json'), true);
        $allLangs = json_decode(file_get_contents('0_sources/langs.json'), true);
        $langinfo = $allLangs[$lang];
        $localname = $langinfo['local_name'];

        # Write the episode title in local $lang:
        echo '<strong>'.$episodetitle[$lang].'</strong>';
        # Date
        if (isset($episodeinfos['published'])) {
          echo ' <em style="font-size:0.8em;">('._("published on").' '.$episodeinfos['published'].')</em>';
        }

      }
    }
  }

}

# Print credits for the universe/wiki
# for a $lang and a $episode.
function _print_hereva() {

  global $wiki;

  $technicalinfos = json_decode(file_get_contents($wiki.'/info.json'), true);
  echo ''._("Creation:").' ';
  _print_translatorinfos($technicalinfos['hereva-world-credits']['creation'], ", ", ".");
  echo '<br/>';
  echo ''._("Lead Maintainer:").' ';
  _print_translatorinfos($technicalinfos['hereva-world-credits']['lead-maintainer'], ", ", ".");
  echo '<br/>';
  echo ''._("Writers:").' ';
  _print_translatorinfos($technicalinfos['hereva-world-credits']['writers'], ", ", ".");
  echo '<br/>';
  echo ''._("Correctors:").' ';
  _print_translatorinfos($technicalinfos['hereva-world-credits']['correctors'], ", ", ".");
  echo '<br/>';
  echo '<br/>';

}

# Print credits for website's translation
function _print_website_translation($lang_input) {

  global $languages_info;

  if (file_exists('po/'.$lang_input.'-credits.json')) {
    echo '  <h3>'._("Website translation:").'</h3>('.$languages_info[$lang_input]['local_name'].')'."\n";
    echo '  <br/>'."\n";

    $technicalinfos = json_decode(file_get_contents('po/'.$lang_input.'-credits.json'), true);
    if (isset($technicalinfos['website-translation-credits'])) {

      if (isset($technicalinfos['website-translation-credits']['translation'])) {
        echo ''._("Translation:").' ';
        _print_translatorinfos($technicalinfos['website-translation-credits']['translation'], ", ", ".");
        echo '<br/>';
      }

      if (isset($technicalinfos['website-translation-credits']['proofreading'])) {
        echo ''._("Proofreading:").' ';
        _print_translatorinfos($technicalinfos['website-translation-credits']['proofreading'], ", ", ".");
        echo '<br/>';
      }

      if (isset($technicalinfos['website-translation-credits']['contribution'])) {
        echo ''._("Contribution:").' ';
        _print_translatorinfos($technicalinfos['website-translation-credits']['contribution'], ", ", ".");
        echo '<br/>';
        echo '<br/>';
      }

    }
  }
}

function _print_global() {

  global $sources;

  $technicalinfos = json_decode(file_get_contents($sources.'/project-global-credits.json'), true);
  echo '<strong>'._("Technical maintenance and scripting:").' </strong><br/>';
  _print_translatorinfos($technicalinfos['project-global-credits']['technic-and-scripting'], ", ", ".");
  echo '<br/>';
  echo '<strong>'._("General maintenance of the database of SVGs:").' </strong><br/>';
  _print_translatorinfos($technicalinfos['project-global-credits']['svg-database'], ", ", ".");
  echo '<br/>';
  echo '<strong>'._("Website maintenance and new features:").' </strong><br/>';
  _print_translatorinfos($technicalinfos['project-global-credits']['website-code'], ", ", "");
  echo '<br/>';
  echo '<br/>';

}

# Print a translator name + optional link as description data item
# Names + links have the format 'Name <http|mailto ...>'
# Links can be omitted for just a name
function _print_translatorinfos($translatorinfos, $separators, $ending) {

  natcasesort($translatorinfos);
  $number = count($translatorinfos);
  $counter = 0;
  foreach ($translatorinfos as $translatorinfo) {
    preg_match('/(.*)\s+\<((http|mailto).*)\>/', $translatorinfo, $matches);
    if (count($matches) == 4) {
      echo '<a href="' . $matches[2] . '">'. filter_var($matches[1], FILTER_SANITIZE_STRING) .'</a>';
    } else {
      echo filter_var($translatorinfo, FILTER_SANITIZE_STRING).'';
    }
  $counter = $counter + 1;
    if ($counter < $number) {
      echo $separators;
    } else {
      echo $ending;
    }
  }
}
