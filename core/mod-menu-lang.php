<?php if ($root=="") exit;

# TODO: link to the documentation: "add a translation"

echo '  <nav class="col nav sml-12 sml-text-center">'."\n";
echo '    <div class="responsive-langmenu">'."\n";
echo ''."\n";

# Common rules in case of webcomic episodes
if ( $mode == 'webcomic' ) {
  # Build a mini list of episode translated on current episode number (found previously on index.php) and $episodes_all_translations (from lib-database.php)
  $lang_for_this_episode = $episodes_all_translations[$episode_number];
}

# Remove languages that have no translations at all ($languages_available is from lib-database.php)
foreach($languages_available as $key => $language) {
  $percent = $all_percent[$language];
  if ($percent < 1) {
    unset($languages_available[$key]);
  }
}

# Menu 1: Shortcut language selector:
# -----------------------------------
# Languages priority list
# (note: based on geo/visits server stats)
$lang_shortcuts = array(0 => "en", 1 => "fr", 2 => "de", 3 => "es",  4 => "it", 5 => "pt",
                        6 => "cs", 7 => "pl", 8 => "ru", 9 => "cn", 10 => "ja", 11 => "fi",
                        12 => "ro", 13 => "mx", 14 => "eo", 15 => "vi", 16 => "nn", 17 => "da",
                        18 => "ca", 19 => "gd", 20 => "kw", 21 => "tp", 22 => "sp" );

# Parse "Accept Language" info from webbrowser.
$accept_languages = array();
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
  # It's a string, parse it
  preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $parse_result);
  if (count($parse_result[1])) {
    # Feed the list with results
    foreach ($parse_result[1] as $string) {
      # trim for only two letter iso and push them in array
      $iso_string = substr($string,0,2);
      array_push($accept_languages, $iso_string);
    }
  }
}
#To debug: input here articifial 'accept language'
#array_push($accept_languages, "si");

# Plug accept_language elements into leading position lang_shortcut (reverse them first)
$accept_languages = array_reverse($accept_languages);
foreach($accept_languages as $lang_input) {
  array_unshift($lang_shortcuts , $lang_input);
}

# Filter if language are supported
foreach($lang_shortcuts as $key => $item) {
  # Check if ISO code is part of Pepper&Carrot choices ($languages_available is from lib-database).
  if(!in_array($item,$languages_available)) {
    unset($lang_shortcuts[$key]);
  }
  # Check if the webcomic episode is translated
  if ( $mode == 'webcomic' ) {
    if(!in_array($item,$lang_for_this_episode)) {
      # Remove it
      unset($lang_shortcuts[$key]);
    }
  # For the full list of webcomics we filter languages if episode 1 ($episodes_all_translations[0]) is not translated.
  } else if ( $mode == 'webcomics' ) {
    if(!in_array($item,$episodes_all_translations[0])) {
      # Remove it
      unset($lang_shortcuts[$key]);
    }
  } else {
    # Filter all languages not part of $website_translations
    if(!in_array($item,$website_translation)) {
      unset($lang_shortcuts[$key]);
    }
    # Filter website languages with lower than 75% translation
    if ( isset($po_translation_percent[$item]) ){
      if ( $po_translation_percent[$item] < 75 ){
        unset($lang_shortcuts[$key]);
      }
    }
  }
}

# Remove double in the list
$lang_shortcuts = array_unique($lang_shortcuts);

# Trim list to 10 first shortcuts
$lang_shortcuts = array_slice($lang_shortcuts, 0, 11);

# Plug $favlang if missing
if (!in_array($favlang,$lang_shortcuts)) {
  # Validate favlang
  if (in_array($favlang,$languages_available)) {
    array_push($lang_shortcuts,$favlang);
  }
}

# Plug $lang if missing
if (!in_array($lang,$lang_shortcuts)) {
  array_push($lang_shortcuts,$lang);
}

# Display lang shortcuts
echo '      <span class="langshortcuts">'."\n";
foreach($lang_shortcuts as $language) {
  # Init
  $class = '';
  $classb = '';
  $title_bookmark = '';
  $link_url = ''.$root.'/'.$language.'/'.$printmode.''.$printcontent.''.$setoption.''.$extension.'';
  # Decorate the menu item for current $lang
  if ( $language == $lang ){
    $class = ' active';
  }

  # Build the label (eg. fr -> Français)
  $langinfo = $languages_info[$language];
  $language_label = $langinfo['local_name'];

  if ($language == $favlang) {
    $classb = ' bookmark';
    # %1$s is the locale code, %2$s is the language name in English
    $lang_shortcut_title = sprintf(_('%1$s/%2$s translation (bookmarked)'), $language, $langinfo['name']);
  } else {
    # %1$s is the locale code, %2$s is the language name in English
    $lang_shortcut_title = sprintf(_('%1$s/%2$s translation'), $language, $langinfo['name']);
  }

  # Display
  echo '        <a class="translabutton'.$class.''.$classb.'" href="'.$link_url.'" title="'.$lang_shortcut_title.'">'."\n";
  echo '          '.$language_label.''."\n";
  echo '        </a>'."\n";
}
echo '      </span>'."\n";
echo ''."\n";

# Drop down big menu button
# Count the number of translations ($languages_available is from lib-database.php)
$total_lang_count = count($languages_available);
echo '      <label for="langmenu" style="display: inline-block;">'."\n";
echo '        <div class="translabutton">'."\n";
echo '          '.sprintf(ngettext('%d language', 'all %d languages', $total_lang_count),$total_lang_count).' <img src="'.$root.'/core/img/dropdown.svg" alt="v"/>'."\n";
echo '        </div>'."\n";
echo '      </label>'."\n";
echo '      <input type="checkbox" id="langmenu">'."\n";
echo '        <ul class="langmenu expanded">'."\n";


# Menu 2: full list with percents:
# --------------------------------

# Define some translatable strings that will be reused.
# This will save execution time on fetching the translations with gettext.
# I am splitting off the website info in case we want to get a completion percentage
# there in the future - this will save retranslation effort.

# Placeholders: %1$s = language name, %2$s = locale code
$label_website_comics_complete = _('%1$s (%2$s): The translation is complete.');
# Website translation is at 100%
$label_website_translated = _('The website has been translated.');
# Website translation needs work
$label_website_translation_in_progress = _('The website is being translated.');
# Website translation is below minimum completion
$label_website_not_translated = _('The website has not been translated yet.');
# Placeholders: %1$s = language name, %2$s = locale code, %3$s = 'The website has not been translated yet.'
$label_only_comics_complete = ('%1$s (%2$s): Comics translation is complete. %3$s');

# loop on all language ($languages_available is from lib-database.php)
foreach($languages_available as $language) {
  # Variable that require cleaning at each loops.
  $class = '';
  $dirhack = '';
  # Link pattern to load all the current option and be at the right place
  $link_url = ''.$root.'/'.$language.'/'.$printmode.''.$printcontent.''.$setoption.''.$extension.'';
  # Load the percent for this lang ($all_percent is from lib-database.php)
  $percent = $all_percent[$language];
  # Load the website percent for this lang ($po_translation_percent is from lib-database.php)
  $website_percent = $po_translation_percent[$language];

  # Decorate the menu item of the current language selected.
  if ( $language == $lang ){
    $class = 'active';
  # Webcomic mode only: disable button with CSS if the translation is missing.
  } else if ( $mode == 'webcomic' ) {
    # If the lang is not part of the list, desactivate it.
    if(!in_array($language,$lang_for_this_episode)) {
      # Check if we are not in case of a corrupted episode name
      # ($not_found is set after various URL test on index.php).
      if ( $not_found === 0 ){
        $class = 'off';
      }
    }
  # For all the webcomics thumbnails; we display the menu as for episode 1 ($episodes_all_translations[0]);
  } else if ( $mode == 'webcomics' ) {
    # If the lang is not part of the list, desactivate it.
    if(!in_array($language,$episodes_all_translations[0])) {
      $class = 'off';
    }
  } else {
  # For all other mode than webcomic, the website's pages:
    if(!in_array($language,$website_translation)) {
      $class = 'off';
    }
  }

  # Build the label for the language (eg. fr -> Français)
  $langinfo = $languages_info[$language];
  # Workaround for jz: label name can't be printed on website yet.
  if ( $language !== 'jz') {
    $language_label = $langinfo['local_name'];
  } else {
    $language_label = $langinfo['name'];
  }

  # Adjust percent for display
  if ( $percent > 1000 ) {
    # Remove the first digit leading this percentage.
    $percent = $percent - 1000;
  }

  # Create language tooltip depending on website and comic translation progress
  $star = '';
  if ($website_percent == 100 && $percent >= 100) {
    # Comics and website are both complete
    $completion_tooltip = sprintf($label_website_comics_complete, $language_label, $language);
    $star = '              <img src="'.$root.'/core/img/lang_award.svg" alt="star" title="'._("The star congratulates a 100&#37; complete translation.").'"/>'."\n";
  } else {
    # Pick website tooltip part
    if ($website_percent == 100) {
      $website_tooltip = $label_website_translated;
    } else {
      # Pick label depending on how good the website translation progress is
      if ($website_percent > $website_translation_cutoff) {
        $website_tooltip = $label_website_translation_in_progress;
      } else {
        $website_tooltip = $label_website_not_translated;
      }
    }
    # Combine website completion statement with comic percentage string to assemble tooltip
    # Placeholders: %1$s = language name, %2$s = locale code, %3$d = percentage (plural controller), %4$s = Sentence explaining website completion status. &#37; is the % symbol
    $percentage_translation = ngettext('%1$s (%2$s): Comics %3$d&#37; translated. %4$s', '%1$s (%2$s): Comics %3$d&#37; translated. %4$s', $percent);
    $completion_tooltip = sprintf($percentage_translation, $language_label, $language, $percent, $website_tooltip);
  }

  # Workaround RTL lang:
  if ( $language == 'ar') { $dirhack = ' dir=auto'; }

  echo '          <li class="'.$class.'"'.$dirhack.'>'."\n";
  echo '            <a href="'.$link_url.'" title="'.$completion_tooltip.'">'."\n";
  echo '              '.$language_label.''."\n";
  echo '              <span class="percent">'.$percent.'%</span>'."\n";
  echo $star;
  echo '            </a>'."\n";
  echo '          </li>'."\n";
}
echo ''."\n";

# Button that save favorite
$langinfo = $languages_info[$lang];
$favorite_lang_label = $langinfo['local_name'];
echo '          <li class="translabutton bookmark">'."\n";
echo '            <a href="'.$root.'/'.$lang.'/setup/set-favorite-language.html">'.sprintf(_("Save %s as favorite language"),$favorite_lang_label).'</a>'."\n";
echo '          </li>'."\n";
echo ''."\n";
$transladocumentationlink = ''.$root.'/'.$lang.'/documentation/010_Translate_the_comic.html';
echo '          <li class="translabutton" style="margin-top: 10px;" dir="auto">'."\n";
echo '            <a href="'.$transladocumentationlink.'">+ '._("Add a translation").'</a>'."\n";
echo '          </li>'."\n";
echo '        </ul>'."\n";
echo ''."\n";

echo '    </div>'."\n";
echo '  </nav>'."\n";
echo ''."\n";
?>
